Notes
-----

This webiste has been created using following mainly these 3 tutorials : 

* [Official flask tutorial](https://flask.palletsprojects.com/en/1.1.x/tutorial/)
* [Learning Flask](https://pythonise.com/series/learning-flask)
* [Mega Tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world)

It aims to provide a simple flask web application with pre-configured Flask extensions.
It can be used as a base for many things by creating plugins without even editing the core app

 Getting started
-------------------------------------

### TL;DR

#### Production

```sh
    
    ##-- Build docker image
    docker build -f dockerfile -t flask_app:MyFlaskApp .
    
    ##-- Run docker image
    docker run --name MyFlaskApp -p 8000:5000 --rm flask_app:MyFlaskApp

```
#### Development

```
    ##-- Create virtual environment
    python3 -m venv env

    ##-- Activate virtual environment
    PS1='$ ' && . "env/bin/activate"
    
    ##-- Install requirements
    pip install -r requirements.txt
    
    ##-- Set up databases
    flask db upgrade
    
    ##-- Run flask
    export FLASK_APP=run.py
    export FLASK_ENV=development
    flask run
```


 Install on linux
-------------------------------------

### Gunicorn

You can easily deploy a simple server using `gunicorn` 
and optionnally `pymysql` if you are not using sqlite as a database
( e.g. want sqlalchemy to connect to MariaDB/MySQL or PostgreSQL )

```sh

  ##-- Clone git repository
  git clone --recursive https://framagit.org/flask-app-base/flaskApp.git -o MyApp

  ##-- Move to cloned repository
  cd MyApp

  ## Install gunicorn
  pip install gunicorn pymysql
  pip install -r requirements.txt
   
  ## Start gunicorn
  gunicorn -b :5000 run:app

```

* `run:app` means that we are using the file _run.py_ to start the application in the _app_ folder
* Option `-b :5000` means that server will be 
* Add `--daemon` option to run in background
* If you installed gunicorn in you python virtual environment, you will need to start gunicorn by using `env/bin/gunicorn`

On a local server you could stop there, your app will be available at 

But you should really consider the easy extra-step of running gunicorn through nginx.

#### Nginx simple setup

Of course you'll need to install nginx

```
    apt-get install nginx
```

Then create the file `/etc/nginx/conf.d/sites-enabled.conf` with the following content :

```conf  

    server {

      listen 8000;
      listen [::]:8000;	

      server_name my_flask_app;
      charset utf-8;
      
      location / {
          proxy_pass http://0.0.0.0:5000;
          proxy_set_header Host $host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }  
      }

``` 

  And finally restart nginx service

``` 
  sytemctl restart nginx

```

You should now be able to access your app through nginx on port *8000*

**TODO:** More detailled setup (https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/)


 Work with flask
-------------------------------------

To test and edit this project in a **development** environement, start by cloning this git repository

```sh

    ##-- Clone git repository
    git clone --recursive https://framagit.org/flask-app-base/flaskApp.git -o MyApp

    ##-- Move to cloned repository
    cd MyApp

    ##-- Create virtual environment
    python3 -m venv env

    ##-- Activate virtual environment
    PS1='$ ' && . "env/bin/activate"

    ##-- Install requirements
    pip install -r requirements.txt
    
    ##-- Set up databases
    flask db upgrade
    
    ##-- Run flask
    export FLASK_APP=run.py
    export FLASK_ENV=development
    flask run
```

That's it !

You can also install `python-dotenv` to store environement variables

```
    pip install python-dotenv
    
    echo "
    FLASK_APP=run.py
    FLASK_ENV=development
    " |tee .flaskenv

```

And once your virtual environment has been created,
you can source the hepler file *env.sh* to start the virtual environment


```
    . env.sh
    flask run

```


 Deploy with docker
-------------------------------------


### Easy installation with docker

Start by cloning this repository  
*Note that it contains submodules so you need the `--recursive` option.*


```sh

    ##-- Clone git repository
    git clone --recursive https://framagit.org/flask-app-base/flaskApp.git -o MyFlaskApp

    ##-- Move to cloned repository
    cd MyFlaskApp 
```

To build and run the docker image you need the docker command.

On a debian based system you can install docker by running this command :

```
  apt-get install -y docker.io
```
( Or most likely `sudo apt-get install -y docker.io` since you need administrator privileges )

And then build and run the docker image

```sh
    
    ##-- Build docker image
    docker build -f dockerfile -t flask_app:MyFlaskApp .
    
    ##-- Run docker image
    docker run --name MyFlaskApp -p 8000:5000 --rm flask_app:MyFlaskApp

```

**TODO:** Mabe two step builds
**TODO:** Instance folder as a volume


### Full server compose with docker-compose

**TODO:** Docker compose with gunicorn + nginx + postgreSQL and persistent data
**TODO:** SSL certificate
