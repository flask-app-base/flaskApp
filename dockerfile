FROM python:3.8

## Create dedicated user 
#RUN adduser --system pyapp  
RUN addgroup pyapp && adduser --system pyapp --group  
WORKDIR /home/pyapp

## Copy app directory, migrations and scripts
COPY app app
COPY migrations migrations
COPY LICENSE*.txt README*.txt CREDITS*.TXT TODO*.TXT ./
COPY run.py config.py babel.cfg boot.sh ./
RUN chmod a+x boot.sh

## Copy requirements file
COPY requirements.txt requirements.txt

## Start virtual environement
RUN python -m venv env

## Upgrade python pip
RUN env/bin/pip install --upgrade pip

## Explicitly install gunicorn and pymysql
RUN env/bin/pip install gunicorn pymysql

## Install python requirements
RUN env/bin/pip install -r requirements.txt

## There is an issue with misaka (markdown) dependancies
## Below command fix it but take much longer to build !!  
# RUN apk add --no-cache --virtual .misaka_deps build-base python3-dev libffi-dev \
#     && env/bin/pip install -r requirements.txt \
#     && apk del .misaka_deps

## Set flask environment app
ENV FLASK_APP run.py

## Make sure user owns the files
RUN chown -R pyapp:pyapp ./
USER pyapp

## Open port 5000
EXPOSE 5000

## Run boot.sh
ENTRYPOINT ["./boot.sh"]

