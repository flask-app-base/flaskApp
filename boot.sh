#!/usr/bin/env sh
# this script starts a gunicorn server in the virual env
# it is used to boot a Docker container
. env/bin/activate

##-- Upgrade database
while true; do
    flask db upgrade
    if [ "$?" -eq "0" ]; then
        break
    fi
    echo Deploy command failed, retrying in 5 secs...
    sleep 5
done
##-- Compile translations
flask translate compile app/core
for p in $(ls "app/plugins"); do
	[ -d "app/plugins/${p}/translations" ] && flask translate compile "app/plugins/${p}"
done

##-- Start gunicorn
exec gunicorn -w 4 -b :5000 --worker-tmp-dir /dev/shm --access-logfile - --error-logfile - run:app
