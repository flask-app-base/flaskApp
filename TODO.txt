Task list
---------------
  
#### IN PROGRESS:

- babel per modules/plugins translations

  
#### TODO:

- Recursively load modules
- Move some of the create app actions to core __init__
- Load default routes if (and only if ) no index has been defined otherwise

- Docker build from venv with unicorn
- Docker build with 
- Docker 2 steps builds to avoid cache
- Docker-compose with instance as volume

- Test bind paths with MariaDB/PostgreSQL instead of sqlite

- Config option : AUTH_REGISTER_ENABLE
- Config option : AUTH_REGISTER_NEED_CONFIRMATION (by mail)

- Better markdown parsing !

- Auth : Setup admin user at first run

- Properly document features

  
#### FIXME:

- How to allow extra modules or plugins to declare their own DB bind  
  even if they don't refer to the app,
  they can't be git submoduleS because of migations folder :(  
  - https://stackoverflow.com/questions/13058800/using-flask-sqlalchemy-in-blueprint-models-without-reference-to-the-app
  - https://stackoverflow.com/questions/6089020/whats-your-folder-layout-for-a-flask-app-divided-in-modules
   
- How to share database between 2 dispatched app  
  (e.g. website as frontend separated app )
  - https://flask.palletsprojects.com/patterns/appdispatch/

  
#### DONE:

- Separated Core module
- Plugins capabilites





