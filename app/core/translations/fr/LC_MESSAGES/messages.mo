��          �               L  <   M  /   �     �     �     �  %   �           -     6     B     R     i     x  9   �     �  	   �     �     �  �    W   �  -   �       *         K  /   X  /   �     �     �     �  .   �           ?  R   N     �     �  +   �  &   �   Check your email for the instructions to reset your password Congratulations, you are now a registered user! Email Invalid username or password Password Please use a different email address. Please use a different username. Register Remember Me Repeat Password Request Password Reset Reset Password Sign In The page you tried to access is only available for admins Username Welcome ! Your password has been reset. [{}] Reset Your Password Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2020-04-24 21:14+0200
PO-Revision-Date: 2020-04-22 17:51+0200
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 Vérifiez vos courriel pour y touver le code de réinitialisation de votre mot de passe Bravo, votre utilisateur a été enregistré! Courriel Mot de passe ou nom d'utilisateur invalide Mot de passe Merci d'utiliser une addresse e-mail differente Merci d'utiliser un nom d'utilisateur different S'enregitrer Se souvenir de moi Repétez le mot de passe Demander une réinitialisation du mot de passe Réinitialisez le mot de passe S'authentifier La page à laquelle vous avez tenté d'accèder est réservée aux administrateurs Nom d'utilisateur Bienvenue ! Votre mot de passe à été réinitialisé. [{}] Réinitialisation du mot de passe 