from functools import wraps
from flask_login import current_user
from flask import g, request, redirect, url_for, flash
from flask_babel import _

def login_admin_only(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
      if current_user.is_authenticated and current_user.is_admin(): 
        return f(*args, **kwargs)
      else:
        flash(_("The page you tried to access is only available for admins"), 'info')

      return redirect(url_for('auth.login', next=request.url))

    return decorated_function

