from flask import Blueprint, render_template
from flask import current_app as app
from flask_plugins import emit_event
from flask_babel import lazy_gettext as _l

bp = Blueprint("default", __name__, template_folder="templates")

##-- ---[ Home ]------------------------

@bp.route("/")
@bp.route("/index")
def index():
  ## Plugins can add custom action to index route
  emit_event("index_route_on_start")

  app.logger.debug('Env is set to {}'.format(app.config.get("ENV")))
  app.logger.debug('App name is {}'.format(app.config.get("APP_NAME")))
  
  ## Plugins can also add actions at the end, like flash alerts
  emit_event("index_route_before_return")

  return render_template("public/index.html", title=_l("Welcome !"),  content=app.config.get("APP_DESC"))

