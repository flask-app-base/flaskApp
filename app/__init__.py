import os
from flask import Flask, current_app, session, request
## Load extensions 
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap
from flask_fontawesome import FontAwesome
from flask_plugins import PluginManager, Plugin, get_enabled_plugins
from flask_babel import Babel, lazy_gettext as _l
from flask_login import LoginManager
from flask_mail import Mail
from flask_misaka import Misaka

## Fix ALTER bug with sqlalchemy and sqlite
## See : https://github.com/miguelgrinberg/Flask-Migrate/issues/61#issuecomment-208131722
from sqlalchemy import MetaData

naming_convention = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(column_0_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}

## Globally accessible objects
db = SQLAlchemy(metadata=MetaData(naming_convention=naming_convention))
migrate = Migrate()
ma = Marshmallow()
bootstrap = Bootstrap()
fa = FontAwesome()
md = Misaka(fenced_code=True, autolink=True, no_intra_emphasis=True, space_headers=True, no_html=True)
plugin_manager = PluginManager()
babel = Babel()
mail = Mail()
login = LoginManager()
login.login_view = 'auth.login'
login.login_message = _l('Please log in to access this page.')


##  Define a class that will be used to register plugins
class AppPlugin(Plugin):
    def register_blueprint(self, blueprint, **kwargs):
        """Registers a blueprint."""
        current_app.register_blueprint(blueprint, **kwargs)

## Create the app instance
def create_app(testing=False):
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__, instance_relative_config=True)
 
    ##-----[ Config ]-----
    ## Dynamically load config 
    
    ## FLASK_ENV environment variable can be "testing", "development"
    ## testing can also be set as an argument
    flask_env = os.environ.get("FLASK_ENV", None)
    app.logger.debug('Env is set to {}'.format(flask_env))
    
    ## Load config object from config file
    if testing or flask_env == "testing":
        app.config.from_object('config.TestingConfig')
    elif flask_env == "development":
        app.config.from_object('config.DevelopmentConfig')
    else:
        app.config.from_object('config.ProductionConfig')

    ## Also load "settings.cfg" from instance, if it exists
    app.config.from_pyfile("settings.cfg", silent=True)

    ## And ensure that the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass
    
    ## BABEL : Find all translations directories and update babel config
    if not app.config.get('BABEL_TRANSLATION_DIRECTORIES'):
      dir_list = []
      name = 'translations'
      for root, dirs, files in os.walk(os.path.dirname(__file__)):
        if name in dirs:
          dir_list.append(os.path.relpath(os.path.join(root, name),os.path.dirname(__file__)))

      app.config['BABEL_TRANSLATION_DIRECTORIES'] = ";".join(dir_list)
      app.logger.debug("Babel translations directories are '{}'".format(app.config.get('BABEL_TRANSLATION_DIRECTORIES')))
 
    ##-----[ Databases config ]-----
    
    ## We use SQLALCHEMY_BINDS to bind multiples databases
    ## Absolute path for SQLALCHEMY_DATABASE_URI can be defined in config files
    ## But if only DB_NAME is provided URI wil be instance relative

    ## Make sure URI for main database exists
    if app.config.get("DB_NAME") and not app.config.get("SQLALCHEMY_DATABASE_URI"):
      ## Create instance relative database
      app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(app.instance_path, app.config.get("DB_NAME") + '.sqlite')

    ## Make sure URI for binds database exists (users)
    if app.config.get("DB_BINDS") and not app.config.get("SQLALCHEMY_BINDS"):
      ## Make sure binds dict exists
      app.config['SQLALCHEMY_BINDS'] = dict()
      ## Assosciate binds with instance relative path
      for bind in app.config.get("DB_BINDS"):
        app.config['SQLALCHEMY_BINDS'].update({  
          bind : 'sqlite:///' + os.path.join(app.instance_path, '{}.sqlite'.format(bind))
        })
    
    ##-----[ Extensions ]-----

    ## This were we initlize sqlAlchemy, migrate, babel, login...
    db.init_app(app)
    migrate.init_app(app, db, render_as_batch=True)
    ma.init_app(app)
    bootstrap.init_app(app)
    fa.init_app(app)
    md.init_app(app)
    plugin_manager.init_app(app)
    babel.init_app(app)
    login.init_app(app)
    mail.init_app(app)
    
    ##-----[ App context ]-----
    with app.app_context():

      ##-----[ Route ]-----
      ## Just an example of a minimal route 
      ## Commented this index route is now provided by main module
      
      # @app.route("/index")
      # @app.route("/")
      # def index():
          # app.logger.debug('Env is set to {}'.format(app.config.get("ENV")))
          # app.logger.debug('App name is {}'.format(app.config.get("APP_NAME")))
          # return "Hello, World!"
          
      ##-----[ Blueprints ]-----
      
      ## Authentication
      ## (Moslty copied from : https://github.com/miguelgrinberg/microblog/)
      from app.core.auth import bp as auth_bp
      app.register_blueprint(auth_bp, url_prefix='/auth')
 
      ## Check if a plugin named default is enabled
      ## before loading 'default' blueprint
      enabled_plugins = [ v.identifier for v in get_enabled_plugins() ]
      if not 'default' in enabled_plugins:
        ## Default application
        from app.core.default import bp as default_bp 
        app.register_blueprint(default_bp, url_prefix="/")


      ##-----[ Redirecting rules ]-----

      ## This make url_for('index') be the same as url_for('main.index')
      ## As long as main blueprint url_preifx is "/"
      app.add_url_rule("/", endpoint="index")
      app.add_url_rule("/index", endpoint="index")

      return app


@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(current_app.config['LANGUAGES'])
    return 'fr'

