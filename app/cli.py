import os
import click


def register(app):
    @app.cli.group()
    def translate():
        """Translation and localization commands."""
        pass

    @translate.command()
    @click.argument('lang')
    @click.argument('path')
    def init(lang,path='app'):
        """Initialize a new language for specified folder."""
        babel_cfg = os.path.join(path,'babel.cfg')
        messages_pot = os.path.join(path,'messages.pot')
        translations = os.path.join(path,'translations')
        
        ## Write babel.cfg file
        with open(babel_cfg, 'w+') as babel_file:
          cfg  = "[python: {}/**.py]".format(path)
          cfg += "[jinja2: {}/templates/**.htm]".format(path)
          cfg += "[jinja2: {}/templates/**.html]".format(path)
          cfg += "extensions=jinja2.ext.autoescape,jinja2.ext.with_"
          babel_file.write(cfg)

        ## Extract strings
        if os.system('pybabel extract -F {} -k _l -o {} .'.format(babel_cfg,messages_pot)):
            raise RuntimeError('extract command failed')
        ## Write lang file
        if os.system('pybabel init -i {} -d {} -l {}'.format(messages_pot,translations,lang)):
            raise RuntimeError('init command failed')
        os.remove(messages_pot)

    @translate.command()
    @click.argument('path')
    def update(path='app'):
        """Update all languages for specified folder."""
        babel_cfg = os.path.join(path,'babel.cfg')
        messages_pot = os.path.join(path,'messages.pot')
        translations = os.path.join(path,'translations')
        if os.system('pybabel extract -F {} -k _l -o {} .'.format(babel_cfg,messages_pot)):
            raise RuntimeError('extract command failed')
        if os.system('pybabel update -i {} -d {}'.format(messages_pot,translations)):
            raise RuntimeError('update command failed')
        os.remove(messages_pot)

    @translate.command()
    @click.argument('path')
    def compile(path='app'):
        """Compile all languages for specified folder."""
        translations = os.path.join(path,'translations')
        if os.system('pybabel compile -d {}'.format(translations)):
            raise RuntimeError('compile command failed')
