###
### ---------------------------------------------------------------
### WARNING : 
### ---------------------------------------------------------------
###
###   You should not edit this file.
###   You are probably looking for "settings.cfg"
###   locateted in the instance directory
###   This one contains only default values and is overriden
###   by "settings.cfg" wich is also the one used by 
###   the web interface (configuration menu in the app)
###
### ---------------------------------------------------------------

import os

basedir = os.path.abspath(os.path.dirname(__file__))
 
class Config(object):
  DEBUG = False
  TESTING = False
  BASEDIR = basedir

  ## Application infos
  APP_NAME = "App Name"
  APP_DESC = "This app is powered by flask ... ! "

  ## Cookies configuration
  SESSION_COOKIE_SECURE = True

  ## Langages available
  LANGUAGES = ['fr','en']

  ## -----[ Bootstrap Options ]----- 

  ## Use local references
  BOOTSTRAP_SERVE_LOCAL = True
  ## Set to false for more readable css
  BOOTSTRAP_USE_MINIFIED = True
  
  ## -----[ Databases Options ]----- 

  ## Set database name for instance relative database
  DB_NAME = "database"
  DB_BINDS = [ "users" ]
  # DB_USERNAME = "admin"
  # DB_PASSWORD = "password"

  ## Sessions secret key from env or use random if it doesn't exist 
  SECRET_KEY = os.environ.get('SECRET_KEY') or os.urandom(24)

  ## Or directly use SQLAlchemy variables
  # SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'database.sqlite')
  # SQLALCHEMY_DATABASE_BINDS = {
    # "users" : 'sqlite:///' + os.path.join(basedir, 'users.sqlite')
  # }
  SQLALCHEMY_TRACK_MODIFICATIONS = False

  
class ProductionConfig(Config):
    pass

class DevelopmentConfig(Config):
  DEBUG = True
  SQLALCHEMY_TRACK_MODIFICATIONS = True
  # SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app-dev.db')

class TestingConfig(Config):
  TESTING = True
  SESSION_COOKIE_SECURE = False
  SQLALCHEMY_TRACK_MODIFICATIONS = True
  # SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app-dev.db')
