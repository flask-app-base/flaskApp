#! /bin/bash

## https://unix.stackexchange.com/questions/4650/determining-path-to-sourced-shell-script
#echo '$_ : '$_
#echo '$0 : '$0
#echo '$BASH_SOURCE : '$BASH_SOURCE

script='' 
parent="."
venv="env"

if [ -n "$BASH_SOURCE" ]; then
  script="$BASH_SOURCE"
else
  echo "You should run this script using 'source $0'"
  exit
fi

[ -n "$script" ] && parent=$(dirname "$script")

firstrun=0

if [ -d "${parent}" ] && [ ! -d "${parent}/${venv}" ]; then
  echo "It looks like the virtual env doesnt exist yet (${parent}/${venv}) "
  read -p 'Create and init the virtual environement ? [Y]/n ' res
  case $res in 
    ''|y|yes|Y|Yes|YES|o|O) firstrun=1 ;;
    *) firstrun=0 ;;
  esac
fi
echo "$firstrun"

if [ -z "$VIRTUAL_ENV" ] && [ "$firstrun" -eq 1 ]; then 
    sudo apt-get install python3-venv || return 1
    sudo apt-get install build-essential python-dev libffi-dev || return 1
    python3 -m venv env || return 1
fi


if [ ! -f "$parent/.flaskenv" ]; then
    echo "## Flask environment variables : " |tee "$parent/.flaskenv"
    echo "FLASK_APP=run.py" |tee -a "$parent/.flaskenv"
    echo "FLASK_ENV=development" |tee -a "$parent/.flaskenv"
fi

if [ -d "${parent}" ] && [ -d "${parent}/${venv}" ]; then
  echo "... Moving to folder '$parent' ..."
  cd "$parent"
  echo "... Redefining PS1..."
  echo "... Activating virtual environement at '${venv}/bin/activate' ..."
   PS1='$ ' && . "${venv}/bin/activate" 
  echo "... Done"
else
  echo "Error: Something went wrong when trying to detect the folder : '${parent}/${venv}'"
  echo "	if the virtual environement doesn't exist, run the following command before this script :"
  echo "		python3 -m venv \"${parent}/${venv}\""
  echo ""
  echo "  you will then need to install requirements :"
  echo "    pip install --upgrade wheel"
  echo "    pip install --upgrade pip python-dotenv pipreqs"
  echo "    pip install -r requirements.txt"
  echo ""
fi


if [ -n "$VIRTUAL_ENV" ] && [ "$firstrun" -eq 1 ]; then 
    pip install --upgrade wheel || return 1
    pip install --upgrade pip python-dotenv pipreqs || return 1
    # pipreqs --force --ignore="/app/plugins" "." || return 1
    # sed -i "s#^WTForms==\(.*\)\$#WTForms[email]==\1#" requirements.txt || return 1return 1
    pip install -r requirements.txt || return 1
    flask db upgrade || return 1
fi

#PS1='$ '
#source env/bin/activate
